### 基于Socket和Swing的桌面端聊天软件


![输入图片说明](https://images.gitee.com/uploads/images/2021/0402/094849_bbc196a4_1368059.png "QQ图片20210402094829.png")

表格详细设计
本系统一共设计了十张表，分别是用户表（user）、用户信息表（user_info）、用户状态表(user_status)、私聊消息关联表（personal_chat_relation）私聊消息内容表(personal_chat_mesg)、群表(group)、群聊信息表(group_info)、群聊用户表(group_chat_list)、群消息关联表(group_chat_relation)、群消息内容表(group_chat_mesg)。
各表结构如下:
(1) 用户表（user）用于存储用户ID和手机号，并将ID与用户信息表关联。
表3.6用户表
字段名	类型	长度	说明
ID	bigint	36	独立字段自增长主键字段

续表3.6
u_phone	varchar	20	用户手机号

(2) 用户信息表（user_info）用于存储用户姓名、密码、部门列表、在线状态等信息。
表3.7用户信息表
字段名	类型	长度	说明
ID	bigint	36	用户表外键
u_name	varchar	20	用户名
u_password	varchar	20	用户密码
u_dept	varchar	200	部门列表

(3) 用户状态表（user_status）用于存储用户在线状态。
表3.8用户状态表
字段名	类型	长度	说明
ID	bigint	36	用户表外键
u_status	int	2	用户在线状态

(4) 私聊消息关联表（personal_chat_relation）用于存储私聊消息的发送时间、发送用户、接收用户等信息。
表3.9私聊消息关联表
字段名	类型	长度	说明
ID	bigint	36	主键
u_send	bigint	36	用户表外键,发送者ID
u_receive	bigint	36	用户表外键,接收者ID
time	date	—	发送时间

(5) 私聊消息内容表（personal_chat_mesg）用于存储私聊消息的内容、发送用户名等信息。

表3.10私聊消息内容表
字段名	类型	长度	说明
ID	bigint	36	主键
r_id	bigint	36	私聊关联表外键,关联ID
u_name	varchar	20	发送者用户名
mesg	text	500	发送的消息

(6) 群表（group）用于存储群基本信息。
表3.11群表
字段名	类型	长度	说明
ID	bigint	36	群ID
g_name	Varchar	20	群名

(7) 群聊信息表（group_info）用于关联群ID、群主ID、群用户表ID。
表3.12群聊信息表
字段名	类型	长度	说明
ID	bigint	36	群表外键,群ID
u_id	bigint	36	用户表外键,用户ID
g_list_id	bigint	36	群用户表外键,群用户表ID

(8) 群聊用户表（group_chat_list）用于存储群内用户成员信息。
表3.13群聊用户表
字段名	类型	长度	说明
ID	bigint	36	主键
u_id	bigint	36	用户表外键,用户ID
g_list_id	bigint	36	群用户表外键,群用户表ID

(9) 群聊消息关联表（group_chat_relation）用于存储群聊消息信息。
表3.14群聊消息关联表
字段名	类型	长度	说明
g_id	bigint	36	群表外键,群ID
u_id	bigint	36	用户表外键,用户ID
time	date	—	时间

(10) 群聊消息内容表（group_chat_mesg）用于存储聊内容。
表3.15群聊信息表
字段名	类型	长度	说明
id	bigint	36	主键
g_id	bigint	36	群聊消息关联表外键
mesg	text	500	消息内容
u_name	varchar	20	发送者名称
